Temat 2 (Analiza mikrodanych): „Symulacja reformy podatkowej”
	
Na podstawie załączonego zbioru danych (dane_dochody.csv) proszę o wykonanie poniższych zadań. Opis zbioru danych i wytyczne do zadań znajdują się na końcu polecenia.
W pewnym państwie mieszkańcy uzyskują dochody wyłącznie z trzech źródeł: pracy najemnej, działalności gospodarczej i sprzedaży papierów wartościowych. Dochody z pracy najemnej są opodatkowane w inny sposób niż dochody z działalności gospodarczej i dochody kapitałowe. 

Dochody z pracy najemnej opodatkowane są według progresywnej skali podatkowej, gdzie x to podstawa opodatkowania:
W przedziale x ∈[0;10 000) krańcowa stawka podatkowa wynosi 0%
W przedziale x ∈[10 000;70 000) krańcowa stawka podatkowa wynosi 20%
W przedziale x ∈[70 000;∞) krańcowa stawka podatkowa wynosi 30%
Dochody z działalności gospodarczej i dochody kapitałowe są opodatkowane liniową stawką 20%.
 
Podstawa opodatkowania stanowi sumę dochodów tylko z tych źródeł, z których podatnik osiągnął nieujemny dochód.
W kolejnym roku planowana jest następująca reforma podatkowa: pracownicy najemni skorzystają z dodatkowej ulgi podatkowej maksymalnie w wysokości 1000 jednostek pieniężnych rocznie. Ulga pomniejszy podatek wyłącznie na skali podatkowej. Część niewykorzystanej ulgi przepada (jeżeli nie wystarcza podatku, aby odliczyć całą ulgę). Reforma ma być w całości sfinansowana poprzez dodanie progu podatkowego, powyżej którego krańcowa stawka podatkowa wyniesie 40%. Podatek będzie obliczany od nadwyżki sumy nieujemnych dochodów ze wszystkich źródeł ponad tym progiem.

Zadania:
1. Ile wynoszą dochody z podatku dochodowego przed wprowadzeniem reformy?
2.  Na jakim poziomie powinien być ustalony próg podatkowy, aby reforma była neutralna dla sektora finansów publicznych (nie generowała kosztów, ani zysków dla sektora finansów publicznych), przy założeniu braku efektów behawioralnych? Proszę podać wysokość progu podatkowego zaokrągloną do najbliższej wartości całkowitej.
3. Zaproponuj i wykonaj wizualizację efektu redystrybucyjnego reformy.
4.  Wypowiedz się na następujący temat: Czy rozwarstwienie dochodowe i majątkowe negatywnie oddziałuje na wzrost gospodarczy? Odpowiedź nie powinna przekroczyć 300 słów (bez bibliografii).

Opis zbioru danych. 
Zbiór danych do obliczeń znajduje się w pliku dane_dochody.csv. 

Objaśnienia do zmiennych:
id_podatnika - numer identyfikacyjny podatnika
rok - rok podatkowy
doch_praca - dochód z pracy najemnej
doch_dg - dochód z działalności gospodarczej
doch_gielda -  dochód ze sprzedaży papierów wartościowych

Rekomendowanym oraz dodatkowo punktowanym narzędziem do obliczeń jest środowisko R a do opracowania raportu z odpowiedziami RMarkdown (https://rmarkdown.rstudio.com/). Raport wraz z wizualizacją nie powinien być dłuższy niż 3 strony. Rozwiązanie należy przesłać w formacie pdf lub html wygenerowanym w RMarkdown. Jeżeli obliczenia zostały wykonane w R/RMarkdown należy również przesłać plik .rmd z raportem i/lub skrypt R. 

