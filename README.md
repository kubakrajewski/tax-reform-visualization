# Tax reform visualization
This is a project created during a competition organized by the Ministry
of Finance. The goal was to compute optimal parameters and visualize effects 
of a tax reform. Detailed description of the task can be found in the file
zadanie.txt.
