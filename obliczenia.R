data <- read.csv("dane_dochody.csv")

#sprawdzenie czy pracownicy mają poprawny dochód
sum(is.na(data))
# sprawdzenie jaka część obserwacji ma ujemny dochód
(length(data$doch_gielda[data$doch_gielda < 0]) + 
    length(data$doch_dg[data$doch_dg < 0])) / dim(data)[1]
# to około 5%, bez dużej szkody dla analizy możemy zamienić na 0
data$doch_dg[data$doch_dg < 0] <- 0
data$doch_gielda[data$doch_gielda < 0] <- 0

# dodanie kolumn z podatkami
podatek_praca <- 
  ifelse(data$doch_praca >= 10000 & data$doch_praca < 70000, 
         0.2 * data$doch_praca, 0) +
  ifelse(data$doch_praca >= 70000, 0.3 * data$doch_praca, 0)
podatek_dg <- 0.2 * data$doch_dg
podatek_gielda <- 0.2 * data$doch_gielda 
podatek <- podatek_praca + podatek_dg + podatek_gielda
# dochód z podatku
doch <- sum(podatek)

# funkcja tworzy wektor z wartością nowej daniny dla progu wys. x
podatek_n <- function(x){
  s <- data$doch_praca + data$doch_dg + data$doch_gielda
  return(pmax(0.4 * (s - x), 0))
}
# wartość podatku od pracy po reformie
podatek_praca_ref <- 
  ifelse(data$doch_praca >= 10000 & data$doch_praca < 70000, 
         (0.2 * data$doch_praca) - 1000, 0) +
  ifelse(data$doch_praca >= 70000, (0.3 * data$doch_praca) - 1000, 0)

# Funkcja której wartość będziemy minimalizować
opt <- function(x){
  return( abs( sum(podatek_n(x) + podatek_praca_ref + 
                     podatek_dg + podatek_gielda ) - doch) )
}

# Maksymalna możliwa wartość progu, którą trzeba sprawdzić
m <- max(data$doch_praca + data$doch_dg + data$doch_gielda)

# Znalezienie minimalnej wartości i zamiana prog na liczbę całkowitą
# Funkcja opt jest unimodalna, więc można zastosować optimize
prog <- optimize(opt, c(0, m), tol = 0.5)
prog <- round( unlist(prog)[1] )

# dodanie kolumn z dochodem rozporządzalnym
podatek_n <- podatek_n(prog)
podatek_ref <- podatek_praca_ref + podatek_dg + 
  podatek_gielda + podatek_n
data$doch_roz <- data$doch_praca + data$doch_dg + data$doch_gielda - 
  podatek
data$doch_roz_n <- data$doch_praca + data$doch_dg + data$doch_gielda - 
  podatek_ref

# Podsumowanie
# Wpływ z podatku przed reformą
print(doch)
# Optymalna wartość progu
print(prog)
# Wpływ z podatków po reformie
print(sum(podatek_ref))
# Zapis danych
write.csv(data, "dane_dochody_ref.csv")